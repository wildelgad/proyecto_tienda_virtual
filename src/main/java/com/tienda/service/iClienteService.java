package com.tienda.service;

import java.util.List;

import com.tienda.model.Cliente;

public interface iClienteService {

	Cliente create(Cliente cliente);
	
	Cliente update(Cliente cliente);
	
	Cliente findById(Integer id);
	
	List<Cliente> findAll();

	void delete(Integer id);
}
